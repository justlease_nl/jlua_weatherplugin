![weatherplugin.png](https://bitbucket.org/repo/kAyM7p/images/410278638-weatherplugin.png)

# Weather plugin
***Weather plugin for use with Universal Analytics***

*****

This plugin is a rewrite version of the plugin as mentioned on [www.simoahava.com](http://www.simoahava.com/web-development/universal-analytics-weather-custom-dimension/). It has been rewritten because the original plugins needs jQuery. This version is just in pure javascript.


## How it works
1. Create a new tag in the tag manager 
2. Name the tag “Weather API”
3. Choose Custom HTML Tag as the tag type
4. Add the content of the mL file within the html area
5. Add rule to fire tag on every page *({{url}} matches RegEx .\*)*

For all the other stuff about the plugin check out the above mentioned blogpost.